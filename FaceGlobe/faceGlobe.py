#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  faceGlobe.py
#  
#  Copyright 2013 ryan <ryan@porta-penguin>
#  

import random, urllib, httplib, webbrowser
from urlparse import urlparse 

def main():
	print geocode('1600 Amphitheatre Parkway, Mountain View, CA 94043, USA')
	return 0
	
def geocode(address):
	googleGeocodeUrl = 'http://maps.google.com/maps/geo?'
	parms = {
		'output': 'csv',
		'q': address}

	url = googleGeocodeUrl + urllib.urlencode(parms)
	resp = urllib.urlopen(url)
	resplist = list(resp)
	line = resplist[0]
	status, accuracy, latitude, longitude = line.split(',')
	return latitude, longitude
	
def disjoint():
	pass

if __name__ == '__main__':
	main()

