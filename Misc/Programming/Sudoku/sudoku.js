var board = [
[0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0]];

/*
var testboard = [
[5,9,1,7,6,1,9,2,8],
[2,8,4,8,4,3,7,1,4],
[3,7,6,9,2,5,6,5,3],
[6,2,3,6,5,2,1,4,8],
[8,5,9,9,3,8,3,7,6],
[7,4,1,1,7,4,5,2,9],
[8,7,1,6,3,4,4,8,2],
[5,6,4,2,1,9,3,5,7],
[2,3,9,7,5,8,6,9,1]];
*/

var main = function(){
	fillBoard();
	//printBoard();
}

var printBoard = function(){
	for(var i = 0; i < 9; i++){
		for(var j = 0; j < 9; j++){
			document.write(board[i][j]);
			if(((j + 1) % 3 === 0) && j !== 8){
				document.write("|");
			}
			else{
				document.write(" ");
			}
		}
		document.write("\n");
		if((i + 1) % 3 === 0 && i !== 8){
				document.write("-----------------\n");
		}
	}
}

var fillBoard  = function(){ // this funcrion fills cells in blocks of 9, and progresses down the board.
	var iMod = 0; // modifications made for i and j to account for the modified counting structure
	var jMod = 0; // ditto
	
	for(var i = 0; i < 9; i++){
		var oneToNine = [1, 2, 3, 4, 5, 6, 7, 8, 9];
		var jAdd = (Math.floor(i / 3) * 3);
		for(var j = 0; j < 9; j++){
		
			iMod = ((Math.floor(j / 3) + (3 * i)) % 9); // the i modification
			jMod = jAdd + (j % 3); // the j modification

			var pickOne = oneToNine.splice((Math.floor(Math.random() * oneToNine.length)), 1);
			
			// THIS IS WHERE I THINK IT'S NOT WORKING
			while(!isValid(pickOne, iMod, jMod)){ // keep trying until a valid number is picked for the cell.
				if(j == 8){
					j = 0;
					oneToNine = [1, 2, 3, 4, 5, 6, 7, 8, 9];
					board[iMod][jMod] = 0;
					board[iMod][jMod - 1] = 0;
					board[iMod][jMod - 2] = 0;
					board[iMod - 1][jMod] = 0;
					board[iMod - 1][jMod - 1] = 0;
					board[iMod - 1][jMod - 2] = 0;
					board[iMod - 2][jMod] = 0;
					board[iMod - 2][jMod - 1] = 0;
					board[iMod - 2][jMod - 2] = 0;
					pickOne = oneToNine.splice((Math.floor(Math.random() * oneToNine.length)), 1);
				}
				else{
					addToArray(oneToNine, pickOne);
					pickOne = oneToNine.splice((Math.floor(Math.random() * oneToNine.length)), 1);
				}
			}
			setBoard(iMod, jMod, pickOne);
			// VVV TESTING PURPOSES VVV
			printBoard();
			document.writeln("\n/////////////////\n");
		}
	}
}

var isValid = function(val, indx, indy){
	var lookRight = 0;
	var lookDown = 0;
	for(var k = 0; k < 9; k++){
		lookRight = parseInt(board[indx][k]);
		lookDown = parseInt(board[k][indy]);
		// v for testing purposes v
		document.writeln("val = " + val + ", board[i] = " + lookRight + ", board[j] = " + lookDown + ", k = " + k);
		
		if((val == lookRight) || (val == lookDown)){
			document.writeln("Collision!");
			return false;
		}
	}	
	return true;
}

var addToArray = function(arr, val){
	arr[arr.length] = val;
	return arr;
}

var arrContains = function(arr, val){
	for(var i = 0; i < arr.length; i++){
		if(arr[i] == val){
			return true;
		}
	}
	return false;
}

var setBoard = function(intx, inty, val){
	board[intx][inty] = val;
}

main();
