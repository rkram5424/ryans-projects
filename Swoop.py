# Authors: Ryan Kramlich and maybe Julie Faircloth
# Just a little script to make life MUCH easier

import urllib2, urllib, re, os
from os.path import expanduser

root_url_old = 'http://apps.acesag.auburn.edu/mediamax/'
home = expanduser("~")
root_path = home + '/Documents/Work/Swoop/'
category_array = ['Aquaculture', 'Aquaculture: Catfish', 'Aquaculture: Countries', 'Aquaculture: Crustaceans',
'Aquaculture: Mollusks', 'Aquaculture: Tilapia', 'Departmental Activities', 'Facilities: Aquaculture', 
'Facilities: Departmental', 'Faculty:Staff:Students', 'Fish Diseases', 'Fisheries Biology', 'Hatchery Management', 
'High School Aquaculture', 'Historical', 'Historical: Homer Swingle', 'International Projects', 'Limnology', 
'Other Aquatic Organisms', 'Other Coastal Activities', 'Recreational Fishing', 'World Ichthyology']
category_num_array = [26,38,37,41,42,55,56,58,28,60,54,27,29,36,23,24,59,32,57,30,25,35]

def main():

	# ONE-TIME CODE
	# This creates a folder for each category
	# I'm keeping it for archiving purposes
	# for i in range(len(category_array)):
	# 	newpath = root_path + category_array[i]
	# 	if not os.path.exists(newpath): os.makedirs(newpath)

	for i in range(len(category_num_array)):
		directory = root_path + category_array[i]
		for j in range(1,20):
			root_url = root_url_old + 'browse/categories/pictures/z/'
			root_url  = root_url + str(category_num_array[i]) + '/' + str(j) + '/'
			for k in range(len(get_urls(root_url))):
				swoop_page(get_urls(root_url))

	print get_urls('http://apps.acesag.auburn.edu/mediamax/browse/categories/pictures/z/26/1/Aquaculture')

	return 0

def get_urls(url):
	url_array = []
	page = urllib2.urlopen(url)
	data = page.read()

	url_count = data.count('http://apps.acesag.auburn.edu/mediamax/pictures/')
	for i in range(url_count):
		if (i % 2 == 1):
			url_array.append(string_sandwich(data, 'http://apps.acesag.auburn.edu/mediamax/pictures/', '.html'))
		data = data.replace('http://apps.acesag.auburn.edu/mediamax/pictures/','done',1)
	return url_array

def string_sandwich(s, top_bun, bottom_bun):
	try:
		start = s.index( top_bun )
		end = s.index( bottom_bun, start )  + len( bottom_bun )
		return s[start:end]
	except ValueError:
		return ""

def find_between(s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def swoop_page(url, directory):
	os.chdir(directory)

	image_address = ''
	title = ''
	description = ''
	category = ''
	tag_code = ''
	tag_string = ''
	tags = []

	page = urllib2.urlopen(url)
	data = page.read()

	# IMAGE
	pre_string = '''</a>" href="'''
	image_address = find_between(data, pre_string, '" rel="lightbox"')
	k = image_address.rfind('/')
	image_name = image_address[k+1:]
	urllib.urlretrieve(image_address, image_name)

	file_name = image_name.replace('.jpg','')
	f = open(file_name + '.txt', 'w')

	# TITLE
	pre_string = 'Title:</font>'
	title = find_between(data, pre_string, '<br>')

	# DESCRIPTION
	pre_string = '''<span class="metaLabel">Description: </span>
            <var>'''
	description = find_between(data, pre_string, '</var>')

	# CATEGORY
	pre_string = '''<span class="metaLabel">Category: </span>
            <var>
            
                        '''
	category = find_between(data, pre_string, '''</A>
                        </var><br />''')
	category = category.split('>', 1)[-1]

	# TAGS
	pre_string = '''<span id="tagsBoxInner">
            	
                                '''
	tag_code = find_between(data, pre_string, '''</span>
			</span>''')
	tag_number = tag_code.count('name=')
	for i in range(tag_number):
		tags.append(find_between(tag_code, 'name="', '"'))
		tag_code = tag_code.replace('name', 'done', 1)
		tags[i] = tags[i].replace(',', '')

	f.write(image_address + '\n')
	f.write(title) + '\n'
	f.write(description + '\n')
	f.write(category + '\n')
	for i in range(5):
		comma = ''
		if i > 0:
			comma = ','
		tag_string += (comma + tags[i])
	f.write(tag_string)

if __name__ == '__main__':
	main()
