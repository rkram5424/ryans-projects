sudo add-apt-repository ppa:gottcode/gcppa
# sudo add-apt-repository ppa:kilian/f.lux
sudo add-apt-repository ppa:kxstudio-team/ppa
sudo add-apt-repository ppa:libreoffice/ppa
# sudo add-apt-repository ppa:me-davidsansome/clementine
sudo add-apt-repository ppa:nilarimogard/webupd8
sudo add-apt-repository ppa:otto-kesselgulasch/gimp
# sudo add-apt-repository ppa:team-xbmc/ppa
sudo add-apt-repository ppa:webupd8team/java
sudo add-apt-repository ppa:webupd8team/sublime-text-2
sudo add-apt-repository ppa:webupd8team/themes
# sudo add-apt-repository ppa:xubuntu-dev/xfce-4.10
# sudo add-apt-repository ppa:xubuntu-dev/xfce-4.12

sudo apt-get update
sudo apt-get install aisleriot alsa-base anki armagetronad audacious audacity bleachbit blender brasero calibre catfish chromium-browser clementine comix conky-all cuneiform curl dff emacs espeak espeak-gui evince faenza-icon-theme file-roller g++ gdebi geany geany-plugins gimp gimp-plugin-registry git gnome-cards-data gnome-system-tools gparted hedgewars htop inkscape john kdegames libreoffice mupen64plus mypaint ndisgtk ndiswrapper* okular openshot oracle-jdk7-installer orage pdftk phatch pidgin ppa-purge pysolfc python3 qalculate qjackctl shimmer-themes stellarium sublime-text supertux supertuxkart tesseract-ocr testdisk unetbootin virtualbox visualboyadvance-gtk vlc volumeicon-alsa wine xbmc xfce4 xfce4-goodies xfce4-whiskermenu-plugin xscreensaver* youtube-dl 
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt-get autoremove

#If you're running 64-bit, uncomment the following line:
sudo apt-get install ia32-libs
