# ServerTCP.py
# Networks Lab 1

import sys, socket
from struct import *

def main():
	if len(sys.argv) != 2:
		print 'Argument Error'
		exit(0)
	HOST = ''
	port_number = sys.argv[1] 

	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.bind((HOST, int(port_number)))
	sock.listen(1)
	req_num = 1
	while 1:
		conn, addr = sock.accept()
		# print 'Connection address:', addr #used for testing
		data = conn.recv(1024)
		if not data: break
		####### Put stuff here ########
		reply = ''
	    # print len(data) #used for testing
	    # print reply		#used for testing

		tml = unpack('H', data[0:2])
		
		rid = unpack('H', data[2:4])
		
		op = unpack('B', data[4])
		op = op[0]

		tml = socket.ntohs(tml[0])
		rid = socket.ntohs(rid[0])
	    #op = socket.ntohs(op[0])
		#print tml 		#used for testing
		#print rid		#used for testing
		#print data[5:tml]	#used for testing
		

		mes = unpack(str(tml - 5) + 's', data[5: tml])
	    
	    

		mes = str(mes)
		mes = mes.split('\\x0')[0]
		mes = mes[2:]
		mes += '\0'

		# print 'Request ID: ' + rid 	#used for testing

		if op == 85: 
			reply = vLength(mes)
			#print reply #used for testing
			data = pack('HHH', socket.htons(tml), socket.htons(rid), socket.htons(reply))
		elif op == 170:
			reply = disemvowel(mes)
			# print reply #used for testing
			data = pack('HH' + str(len(reply)) +'s', socket.htons(tml), socket.htons(rid), reply)
		else:
			print 'Invalid Operation'
		conn.send(data)
		######### End stuff ########### 
	conn.close()
	sock.close()

def vLength(string):
	vowel_array = ['a', 'e', 'i', 'o', 'u']
	vl = 0
	for i in range(len(string)):
		if string[i] in vowel_array:
			vl += 1
	return vl

def disemvowel(string):
	string = string.replace('a', '')
	string = string.replace('e', '')
	string = string.replace('i', '')
	string = string.replace('o', '')
	string = string.replace('u', '')
	return string

if __name__ == '__main__':
	main()